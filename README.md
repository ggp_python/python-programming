# Applied Python Programming for Life Scientists

This course is intended for life scientists without any prior knowledge about bioinformatics/programming. The objective is to provide a sufficient amount of knowledge about Python to solve small problems by writing simple scripts. 
